.. _examples-fast-plot:

Qt GUI Fast Plot
---------------------------
.. image:: /_images/examples-fast-plot-screenshot.png

An improved version of the live-plot demonstrating a very responsive Qt+PyQtGraph GUI with peak detection and range history.

**Source code:** :source:`examples/fast-plot.py`

.. literalinclude:: /../examples/fast-plot.py
