.. _examples-range-doppler:

Qt GUI Range-Doppler Plot
---------------------------
.. image:: /_images/examples-range-doppler-screenshot.png

The Range-Dopppler example shows how the 2πSENSE X1000 series devices can be used to perform FMCW sweeps with very precise and reliable timing.
An entire acquisition of multiple sweeps is processed using a range-doppler algorithm providing a 2D color-coded live image of both distance and recession velocity.

**Source code:** :source:`examples/range-doppler.py`

.. literalinclude:: /../examples/range-doppler.py
