.. _examples-live-plot:

Simple Live-Plotting Script
---------------------------
.. image:: /_images/examples-live-plot-screenshot.png

This example shows a simple live-plotting script with basic fft processing using interactive ``matplotlib``.

**Source code:** :source:`examples/live-plot.py`

.. literalinclude:: /../examples/live-plot.py
