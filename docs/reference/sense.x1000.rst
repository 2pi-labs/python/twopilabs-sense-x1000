.. _section-apiref:

2πSENSE X1000 series API reference
==================================

.. automodule:: sense.x1000
   :members:
   :show-inheritance:

.. toctree::
   :maxdepth: 1

   sense.x1000.scpi_core
   sense.x1000.scpi_sense
   sense.x1000.scpi_initiate
   sense.x1000.scpi_calc
   sense.x1000.scpi_memory
   sense.x1000.scpi_system
   sense.x1000.scpi_trigger
   sense.x1000.x1000_base
   sense.x1000.x1000_scpi
