.. twopilabs-sense-x1000 documentation master file, created by
   sphinx-quickstart on Mon Oct 11 10:56:42 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the x1000 package documentation!
=================================================
This module is designed as a control library for 2πSENSE X1000 series radar devices
manufactured by 2π-LABS GmbH.

It provides a high-level abstraction layer using Python running on Windows, OSX and Linux.

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   introduction
   controlling
   configuring
   examples/examples
   reference/sense.x1000

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
